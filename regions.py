from dict_to_obj import DictAsObj

regions = {
  'nedungudi': DictAsObj({
    'districtCode': '22',
    'talukCode': '04',
    'villageCode': '095',
  }),
  'madambakkam': DictAsObj({
    'districtCode': '03',
    'talukCode': '05',
    'villageCode': '143',
  })
}
