import os
import csv
from bs4 import BeautifulSoup

def file_names():
  file_names = []
  for root, dirs, files in os.walk("/tmp/patta"):
    path = root.split(os.sep)
    [file_names.append(os.path.join('/'.join(path), file)) for file in files]
  return file_names

def get_patta_no(soup):
  spans = soup.find_all('span')
  if len(spans) == 4:
    return spans[1].text.split(':')[1].strip()
  return None

def get_col(soup, col_no):
  tds = soup.find_all('td')
  if len(tds) > 10:
    return tds[col_no].text
  return None

with open('/tmp/patta_summary.csv', mode='w') as summary_file:
  summary_writer = csv.writer(summary_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
  summary_writer.writerow(['SurveyNo', 'SubDiv', 'Patta','Nanchai', 'Punchai'])
  for file in file_names():
    with open(file, 'r') as content:
      print(file)
      soup = BeautifulSoup(content, features="html5lib")
      patta_no = get_patta_no(soup)
      nanchai = get_col(soup, -20)
      punchai = get_col(soup, -18)
      splits = file.split('/')
      subdiv = splits[-1].split('.')[0]
      survey = splits[-2]
      summary_writer.writerow([survey, subdiv, patta_no, nanchai, punchai])