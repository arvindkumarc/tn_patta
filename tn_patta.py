import os
import time
import requests
import xml.etree.ElementTree as ET

from regions import regions

root = '/tmp'

region = regions['nedungudi']
sessionID = 'JSESSIONID=951245365A29B91C0E3915C193EA4559; JSESSIONID=8B933569555D82561867A64890304047'

def create_dir(directory):
  dir = os.path.join(root, directory)
  if not os.path.exists(dir):
    os.makedirs(dir)

def get_subdivisions(survey):
  response = requests.get(f"https://eservices.tn.gov.in/eservicesnew/land/ajax.html?page=getSubdivNo&districtCode={region.districtCode}&talukCode={region.talukCode}&villageCode={region.villageCode}&surveyno={survey}",
              headers={'Content-Type': 'text/plain',
                       'Cookie': sessionID,
                       'Referer': 'https://eservices.tn.gov.in/eservicesnew/land/chittaCheck_en.html?lan=ta',
                       'Sec-Fetch-Dest': 'document',
                       'Sec-Fetch-Mode': 'navigate',
                       'Sec-Fetch-Site': 'same-origin',
                       'Sec-Fetch-User': '?1'
                       })
  tree = ET.fromstring(response.text)
  return [i.text for i in tree.iter('subdivcode')]

def get_patta(survey, subdiv):
  res = requests.post('https://eservices.tn.gov.in/eservicesnew/land/chittaExtract_en.html?lan=ta',
                      params={'lan': 'en'},
                      headers={'Content-Type': 'application/x-www-form-urlencoded',
                               'Cookie': sessionID,
                               'Origin': 'https://eservices.tn.gov.in',
                               'Upgrade-Insecure-Requests': '1',
                               'Referer': 'https://eservices.tn.gov.in/eservicesnew/land/chittaCheck_en.html?lan=en',
                               'Sec-Fetch-Dest': 'document',
                               'Sec-Fetch-Mode': 'navigate',
                               'Sec-Fetch-Site': 'same-origin',
                               'Sec-Fetch-User': '?1'},
                      data=chittaRequest(subdiv, survey))
  if res.status_code == 200:
    return res.text
  else:
    raise AssertionError()


def chittaRequest(subdiv, survey):
  return {'task': 'chittaEng',
          'role': 'null',
          'districtCode': region.districtCode,
          'talukCode': region.talukCode,
          'villageCode': region.villageCode,
          'viewOpt': 'sur',
          'pattaNo': '',
          'surveyNo': survey,
          'subdivNo': subdiv,
          'viewOption': 'view',
          'captcha': 'QRZ5YG'}


def save_html(file, content):
  with open(file, 'w') as out:
    out.write(content)

create_dir('patta')
file_count = 0
for survey in range(2, 200):
  time.sleep(0.5)
  create_dir(f"patta/{survey}")
  for subdiv in get_subdivisions(survey):
    file_count = file_count+1
    time.sleep(0.3)
    print(f"saving {survey} {subdiv}, total: {file_count}")
    save_html(f"{root}/patta/{survey}/{subdiv}.html", get_patta(survey, subdiv))
