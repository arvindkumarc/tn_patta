# tn_patta.py

* This scripts get the patta information for all the provided survey numbers in a given district/taluk.
* Saves the information as html in specified directory separated by each survey number.

# html_to_csv.py

* converts the saved html files to common csv (comma separated value) file extracting only the Nanchai, Punchai and PattaNo.

# summary.R

* reads the saved summary.csv file and groups it by PattaNo to avoid duplicates.